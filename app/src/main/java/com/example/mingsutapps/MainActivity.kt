package com.example.mingsutapps

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random
import kotlin.random.nextInt

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var UserScore = 0
        var ComScore = 0

        user_paper.setOnClickListener {
            val versus = Versus(1)
            val result = versus.versus(versus.user, versus.listRandom)
            changeImage(versus.user, versus.listRandom)
            changeBg(versus.result)
            tv_result.text = versus.result

            when(versus.result){
                "WIN" -> UserScore++
                "LOSE" -> ComScore++
            }
            user_score_result.text = UserScore.toString()
            com_score_result.text = ComScore.toString()
        }
        user_rock.setOnClickListener {
            val versus = Versus(1)
            val result = versus.versus(versus.user, versus.listRandom)
            changeImage(versus.user, versus.listRandom)
            changeBg(versus.result)
            tv_result.text = versus.result

            when (versus.result) {
                "WIN" -> UserScore++
                "LOSE" -> ComScore++
            }
            user_score_result.text = UserScore.toString()
            com_score_result.text = ComScore.toString()
        }
        user_scissors.setOnClickListener {
            val versus = Versus(1)
            val result = versus.versus(versus.user, versus.listRandom)
            changeImage(versus.user, versus.listRandom)
            changeBg(versus.result)
            tv_result.text = versus.result

            when (versus.result) {
                "WIN" -> UserScore++
                "LOSE" -> ComScore++
            }
            user_score_result.text = UserScore.toString()
            com_score_result.text = ComScore.toString()
        }

        btn_reset.setOnClickListener {
            user_choose.setImageResource(android.R.color.transparent)
            com_choose.setImageResource(android.R.color.transparent)
            tv_result.text = ""
            user_score_result.text = "0"
            com_score_result.text = "0"
            UserScore = 0
            ComScore = 0
            bg_result.setImageResource(R.drawable.ic_bg_win_foreground)
        }
    }
    fun changeImage(user: Int, com: Int){
        when(user){
            1 -> user_choose.setImageResource(R.drawable.paper)
            2 -> user_choose.setImageResource(R.drawable.rock)
            3 -> user_choose.setImageResource(R.drawable.scissors)
        }
        when(com) {
            1 -> user_choose.setImageResource(R.drawable.paper)
            2 -> user_choose.setImageResource(R.drawable.rock)
            3 -> user_choose.setImageResource(R.drawable.scissors)
        }
    }
    fun changeBg(result: String){
        when(result) {
            "WIN" -> bg_result.setImageResource(R.drawable.ic_bg_win_foreground)
            "DRAW" -> bg_result.setImageResource(R.drawable.ic_bg_draw_foreground)
            "LOSE" -> bg_result.setImageResource(R.drawable.ic_bg_lose_foreground)
        }
    }
}

class Versus(val user : Int){
    val listRandom = Random.nextInt(1..3)
    var result = ""
    fun versus(user: Int, com: Int): String {
        if (user == com) {
            result = "Draw"
        } else {
            when (user) {
                1 -> {
                    if (com == 2) {
                        result = "Win"
                    }
                    if (com == 3) {
                        result = "Lose"
                    }
                }
                2 -> {
                    if (com == 3) {
                        result = "Win"
                    }
                    if (com == 1) {
                        result = "Lose"
                    }
                }
                3 -> {
                    if (com == 1) {
                        result = "Win"
                    }
                    if (com == 2) {
                        result = "Lose"
                    }
                }
            }
        }
        return result
    }
}